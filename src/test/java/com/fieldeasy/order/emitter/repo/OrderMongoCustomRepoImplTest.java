package com.fieldeasy.order.emitter.repo;

import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.domain.Status;
import com.fieldeasy.order.emitter.exception.ResourceNotFound;
import com.fieldeasy.order.emitter.repo.impl.OrderMongoCustomRepoImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderMongoCustomRepoImplTest {

    private MongoOperations mongoOperations;

    private OrderMongoCustomRepoImpl orderMongoCustomRepo;

    private Order orderBeforeUpdate;
    private Order orderAfterUpdate;

    @Before
    public void setup() {
        mongoOperations = Mockito.mock(MongoOperations.class);
        orderMongoCustomRepo = new OrderMongoCustomRepoImpl(mongoOperations);

        orderBeforeUpdate = new Order();
        orderBeforeUpdate.setStatus(Status.PENDING);
        orderBeforeUpdate.setUpdatedOn(Date.from(Instant.now().minus(10, ChronoUnit.MINUTES)));

        orderAfterUpdate = new Order();
        orderAfterUpdate.setStatus(Status.PROCESSING);
        orderAfterUpdate.setUpdatedOn(Date.from(Instant.now().plus(10, ChronoUnit.MINUTES)));

    }

    @Test
    public void updateOrderHappyPath() {
        UUID id = UUID.randomUUID();
        Query query = getMongoQueryWithId(id);
        Update update = new Update();
        update.set("status", orderBeforeUpdate.getStatus());
        update.set("updatedOn", orderBeforeUpdate.getUpdatedOn());
        when(mongoOperations.findAndModify(query, update, Order.class)).thenReturn(orderBeforeUpdate);
        when(mongoOperations.findById(id, Order.class)).thenReturn(orderAfterUpdate);

        Order res = orderMongoCustomRepo.updateOrder(id, orderBeforeUpdate);

        verify(mongoOperations).findAndModify(query, update, Order.class);
        verify(mongoOperations).findById(id, Order.class);
        assertThat(res).isNotNull();
        assertThat(res.getStatus()).isEqualTo(orderAfterUpdate.getStatus());
        assertThat(res.getUpdatedOn()).isEqualTo(orderAfterUpdate.getUpdatedOn());
    }

    @Test(expected = ResourceNotFound.class)
    public void updateOrderNoOrderFoundForId() {
        when(mongoOperations.findAndModify(any(Query.class), any(Update.class), any())).thenReturn(null);
        orderMongoCustomRepo.updateOrder(UUID.randomUUID(), orderBeforeUpdate);
    }


    private Query getMongoQueryWithId(UUID id) {
        return Query.query(Criteria.where("id").is(id));
    }
}