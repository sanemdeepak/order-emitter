package com.fieldeasy.order.emitter.service;

import com.fieldeasy.order.emitter.domain.Order;

/**
 * Created by sanemdeepak on 12/9/18.
 */
public interface PreProcessor {
    Order processNewOrder(Order order);
}
