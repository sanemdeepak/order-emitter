package com.fieldeasy.order.emitter.service;

import com.amazonaws.services.sqs.model.Message;
import com.lambdista.util.Try;

import java.util.List;

/**
 * Created by sanemdeepak on 12/12/18.
 */
public interface AmazonSQSService {
    void publish(Object order);

    Try<List<Message>> readMessages();

    void delete(String messageId);
}
