package com.fieldeasy.order.emitter.service.impl;

import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.domain.Status;
import com.fieldeasy.order.emitter.service.PreProcessor;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/9/18.
 */
@Component
public class PreProcessorImpl implements PreProcessor {

    private final SecureRandom secureRandom;

    public PreProcessorImpl() {
        try {
            this.secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (Exception exp) {
            throw new RuntimeException(exp);
        }
    }

    private Long getRandomLong() {
        return (long) (10000 + secureRandom.nextInt(90000));
    }


    @Override
    public Order processNewOrder(Order order) {
        order.setStatus(Status.PENDING);
        order.setId(UUID.randomUUID());
        order.setOrderDate(Instant.now());
        return order;
    }
}
