package com.fieldeasy.order.emitter.service.impl;

import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.exception.DuplicateResourceException;
import com.fieldeasy.order.emitter.repo.OrderMongoRepo;
import com.fieldeasy.order.emitter.service.OrderService;
import com.fieldeasy.order.emitter.service.PreProcessor;
import com.lambdista.util.Try;
import com.mongodb.DuplicateKeyException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/9/18.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {
    private final PreProcessor preProcessor;
    private final OrderMongoRepo orderMongoRepo;

    @Override
    public Order create(Order order) throws DuplicateResourceException {

        preProcessor.processNewOrder(order);

        Try<Order> trySave = Try.apply(() -> orderMongoRepo.insert(order));

        if (trySave.isFailure()) {
            if (this.isDuplicateInsert(trySave.failed().get())) {
                throw new DuplicateResourceException();
            }
            throw new RuntimeException(trySave.failed().get());
        }
        return trySave.get();
    }

    @Override
    public Order update(UUID id, Order order) {
        //TODO: consider duplicate updates and multiple threads executing this method
        order.setUpdatedOn(Instant.now());
        return orderMongoRepo.updateOrder(id, order);
    }

    private boolean isDuplicateInsert(Throwable throwable) {
        return throwable instanceof DuplicateKeyException
                || throwable instanceof org.springframework.dao.DuplicateKeyException;

    }

}
