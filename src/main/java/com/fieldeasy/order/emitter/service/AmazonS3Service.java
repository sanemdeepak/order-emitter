package com.fieldeasy.order.emitter.service;

/**
 * Created by sanemdeepak on 2018-12-13.
 */
public interface AmazonS3Service {
    void upload(Object object);
}
