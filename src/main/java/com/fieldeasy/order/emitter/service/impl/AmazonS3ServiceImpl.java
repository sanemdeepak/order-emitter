package com.fieldeasy.order.emitter.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.fieldeasy.order.emitter.service.AmazonS3Service;
import com.fieldeasy.order.emitter.util.Util;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.zip.GZIPOutputStream;

/**
 * Created by sanemdeepak on 2018-12-13.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AmazonS3ServiceImpl implements AmazonS3Service {

    private final AmazonS3 amazonS3;
    private final Util util;
    @Value("${aws.s3.error.bucket-name}")
    private String errorBucketName;

    @Override
    public void upload(Object object) {
        InputStream inputStream;
        byte[] bytes;

        try {
            bytes = zippedBytes(getBytesFromObject(object));
            inputStream = new ByteArrayInputStream(bytes);
        } catch (IOException e) {
            log.error("Error building data stream from object");
            throw new RuntimeException(e);
        }

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(bytes.length);

        amazonS3.putObject(errorBucketName, buildFolderPath(), inputStream, metadata);
    }

    private byte[] zippedBytes(byte[] bytes) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        OutputStream objectOutputStream = new GZIPOutputStream(byteArrayOutputStream);


        objectOutputStream.write(bytes);

        objectOutputStream.flush();
        objectOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }

    private byte[] getBytesFromObject(Object o) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);


        objectOutputStream.writeObject(o);

        objectOutputStream.flush();
        objectOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }


    private String buildFolderPath() {
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("UTC"));

        String year = String.valueOf(localDateTime.getYear());
        String month = String.valueOf(localDateTime.getMonthValue());
        String day = String.valueOf(localDateTime.getDayOfMonth());
        String hour = String.valueOf(localDateTime.getHour());
        String minute = String.valueOf(localDateTime.getMinute());
        String seconds = String.valueOf(localDateTime.getSecond());
        String ms = String.valueOf(localDateTime.get(ChronoField.MILLI_OF_SECOND));

        String path = String.join("/", year, month, day, hour, minute, seconds, ms);

        //returns 2018/12/4/2018-12-4-12.dat
        return path.concat("/").concat(path.replaceAll("/", "-").concat(".gzip"));
    }
}
