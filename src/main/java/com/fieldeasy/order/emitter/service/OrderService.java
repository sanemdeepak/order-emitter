package com.fieldeasy.order.emitter.service;

import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.exception.DuplicateResourceException;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/9/18.
 */
public interface OrderService {
    Order create(Order order) throws DuplicateResourceException;

    Order update(UUID id, Order order);
}
