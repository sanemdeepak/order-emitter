package com.fieldeasy.order.emitter.service.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.*;
import com.fieldeasy.order.emitter.exception.ParameterRequiredException;
import com.fieldeasy.order.emitter.service.AmazonSQSService;
import com.fieldeasy.order.emitter.util.Util;
import com.lambdista.util.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sanemdeepak on 12/12/18.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AmazonSQSServiceImpl implements AmazonSQSService {

    private final AmazonSQS amazonSQS;
    private final Util util;
    @Value("${aws.sqs.latest.url}")
    private String latestMessageSqsUrl;
    @Value("${aws.sqs.update.url}")
    private String updateMessageSqsUrl;
    @Value("${toggle.enable.update-flow}")
    private Boolean enableUpdateFlow;

    @Override
    public void publish(Object order) {
        if (order == null) {
            throw new ParameterRequiredException();
        }
        SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(latestMessageSqsUrl)
                .withMessageBody(util.getStringFromObject(order));

        Try<SendMessageResult> trySend = Try.apply(() -> amazonSQS.sendMessage(sendMessageRequest));

        if (trySend.isSuccess()) {
            log.info("Successfully published order to latest queue: order: {}, messageId: {}", order.toString(), trySend.get().getMessageId());
            return;
        }
        log.error("Failed to send order to latest queue, order: {}, cause: {}", order.toString(), ExceptionUtils.getStackTrace(trySend.failed().get()));
    }

    @Override
    public Try<List<Message>> readMessages() {
        if (enableUpdateFlow) {
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest()
                    .withQueueUrl(updateMessageSqsUrl)
                    .withWaitTimeSeconds(5)
                    .withMaxNumberOfMessages(10);

            Try<ReceiveMessageResult> tryReceive = Try.apply(() -> amazonSQS.receiveMessage(receiveMessageRequest));

            if (tryReceive.isFailure()) {
                return new Try.Failure<>(tryReceive.failed().get());
            }

            return Try.apply(() -> tryReceive.get().getMessages());
        }
        return null;
    }

    @Override
    public void delete(String messageId) {
        DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest()
                .withQueueUrl(updateMessageSqsUrl)
                .withReceiptHandle(messageId);
        amazonSQS.deleteMessage(deleteMessageRequest);
    }
}
