package com.fieldeasy.order.emitter.repo;

import com.fieldeasy.order.emitter.domain.Order;

import java.util.UUID;

/**
 * Created by sanemdeepak on 2018-12-13.
 */
public interface OrderMongoCustomRepo {

    Order updateOrder(UUID id, Order order);
}
