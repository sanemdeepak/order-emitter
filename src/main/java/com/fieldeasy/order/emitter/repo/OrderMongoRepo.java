package com.fieldeasy.order.emitter.repo;

import com.fieldeasy.order.emitter.domain.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/12/18.
 */
@Repository
public interface OrderMongoRepo extends MongoRepository<Order, UUID>, OrderMongoCustomRepo {

}
