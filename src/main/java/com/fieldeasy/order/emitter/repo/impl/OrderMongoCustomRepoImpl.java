package com.fieldeasy.order.emitter.repo.impl;

import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.exception.ResourceNotFound;
import com.fieldeasy.order.emitter.repo.OrderMongoCustomRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by sanemdeepak on 2018-12-13.
 */
@Repository
@RequiredArgsConstructor
public class OrderMongoCustomRepoImpl implements OrderMongoCustomRepo {

    private final MongoOperations mongoOperations;

    @Override
    public Order updateOrder(UUID id, Order order) {

        Query query = Query.query(Criteria.where("id").is(id));
        Update update = getUpdateForOrder(order);

        Optional.ofNullable(mongoOperations.findAndModify(query, update, Order.class))
                .orElseThrow(ResourceNotFound::new);

        return mongoOperations.findById(id, Order.class);
    }

    private Update getUpdateForOrder(Order order) {
        Update update = new Update();
        update.set("status", order.getStatus());
        update.set("updatedOn", order.getUpdatedOn());
        return update;
    }
}
