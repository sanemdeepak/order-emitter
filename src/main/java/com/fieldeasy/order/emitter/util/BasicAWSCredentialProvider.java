package com.fieldeasy.order.emitter.util;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by sanemdeepak on 12/12/18.
 */
@Component
public class BasicAWSCredentialProvider implements AWSCredentialsProvider {

    @Value("${aws.accessKeyId}")
    private String accessKeyId;
    @Value("${aws.secretKey}")
    private String secretKey;


    /**
     * Returns AWSCredentials which the caller can use to authorize an AWS request.
     * Each implementation of AWSCredentialsProvider can chose its own strategy for
     * loading credentials.  For example, an implementation might load credentials
     * from an existing key management system, or load new credentials when
     * credentials are rotated.
     *
     * @return AWSCredentials which the caller can use to authorize an AWS request.
     */
    @Override
    public AWSCredentials getCredentials() {
        return new BasicAWSCredentials(accessKeyId, secretKey);
    }

    /**
     * Forces this credentials provider to refresh its credentials. For many
     * implementations of credentials provider, this method may simply be a
     * no-op, such as any credentials provider implementation that vends
     * static/non-changing credentials. For other implementations that vend
     * different credentials through out their lifetime, this method should
     * force the credentials provider to refresh its credentials.
     */
    @Override
    public void refresh() {

    }
}
