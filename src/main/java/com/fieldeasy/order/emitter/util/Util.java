package com.fieldeasy.order.emitter.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by sanemdeepak on 12/12/18.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class Util {

    private final ObjectMapper mapper;


    public String getStringFromObject(Object object) {

        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (Exception exp) {
            log.error("Object writer threw exception, cause: {}", exp);
            throw new RuntimeException(exp);

        }
    }
}
