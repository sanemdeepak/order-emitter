package com.fieldeasy.order.emitter.exception;

import com.fieldeasy.order.emitter.domain.response.ServiceResponse;
import com.fieldeasy.order.emitter.domain.response.Status;
import org.slf4j.MDC;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by sanemdeepak on 12/12/18.
 */
@ControllerAdvice
public class ConstraintViolationExceptionHandler {


    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ServiceResponse> handleViolations(ConstraintViolationException exp) {

        Set<ConstraintViolation<?>> constraintViolations = exp.getConstraintViolations();

        Map<Object, String> messages = new LinkedHashMap<>();
        constraintViolations.forEach(val -> messages.put(val.getPropertyPath().toString(), val.getMessage()));

        ServiceResponse failedRes = new ServiceResponse();
        failedRes.setRequestId(MDC.get("requestId"));
        failedRes.setTimestamp(Instant.now().toEpochMilli());
        failedRes.setStatus(Status.FAIL);
        failedRes.setData(messages);
        return ResponseEntity.badRequest().body(failedRes);
    }
}
