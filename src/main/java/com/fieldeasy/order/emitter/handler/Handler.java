package com.fieldeasy.order.emitter.handler;

/**
 * Created by sanemdeepak on 2019-02-19.
 */
public interface Handler<C, P> {

    P handle(C data);
}
