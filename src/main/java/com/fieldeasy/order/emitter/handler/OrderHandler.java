package com.fieldeasy.order.emitter.handler;

import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.domain.OrderMessage;
import com.fieldeasy.order.emitter.exception.DuplicateResourceException;
import com.fieldeasy.order.emitter.exception.ParameterRequiredException;
import com.fieldeasy.order.emitter.service.AmazonS3Service;
import com.fieldeasy.order.emitter.service.AmazonSQSService;
import com.fieldeasy.order.emitter.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by sanemdeepak on 2019-02-19.
 */
@Component
@RequiredArgsConstructor
public class OrderHandler implements Handler<OrderMessage, Order> {

    private final AmazonS3Service amazonS3Service;
    private final AmazonSQSService amazonSQSService;
    private final OrderService orderService;

    @Override
    public Order handle(OrderMessage data) {
        if (data == null) {
            throw new ParameterRequiredException();
        }

        Order maybeOrder = Optional.ofNullable(data.getOrder()).orElseThrow(() -> new RuntimeException("No order data"));

        try {
            Order success = this.handleOrder(maybeOrder);
            this.handleSuccess(success);
            return success;
        } catch (Throwable throwable) {
            this.handleFailure(maybeOrder);
            return maybeOrder;
        }
    }

    private Order handleOrder(Order order) {
        try {
            return this.orderService.create(order);
        } catch (DuplicateResourceException dre) {
            return this.orderService.update(order.getId(), order);
        }
    }

    private void handleSuccess(Order order) {
        this.amazonSQSService.publish(order);
    }

    private void handleFailure(Order order) {
        amazonS3Service.upload(order);
    }
}
