package com.fieldeasy.order.emitter.listener.impl;

import com.amazonaws.services.sqs.model.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.domain.OrderMessage;
import com.fieldeasy.order.emitter.handler.Handler;
import com.fieldeasy.order.emitter.listener.QueueListener;
import com.fieldeasy.order.emitter.service.AmazonSQSService;
import com.lambdista.util.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by sanemdeepak on 12/12/18.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class QueueListenerImpl implements QueueListener<Message> {

    private final AmazonSQSService sqsService;
    private final Handler<OrderMessage, Order> orderMessageHandler;
    private final ObjectMapper mapper;

    @Override
    //@Scheduled(fixedDelay = 6000000L)
    public void listen() {
        Try<List<Message>> tryReadOrders = sqsService.readMessages();

        if (!tryReadOrders.isFailure()) {
            tryReadOrders.get().stream().map(this::process).forEach(this::acknowledge);
        }

    }

    @Override
    public Message process(Message message) {
        OrderMessage orderMessage = getOrderFromString(message.getBody());
        this.handleOrderMessage(orderMessage);
        return message;
    }

    @Override
    public void acknowledge(Message message) {
        sqsService.delete(message.getReceiptHandle());
    }


    private Order handleOrderMessage(OrderMessage orderMessage) {
        return orderMessageHandler.handle(orderMessage);
    }

    private OrderMessage getOrderFromString(String s) {
        try {
            return mapper.readValue(s, OrderMessage.class);
        } catch (Exception exp) {
            log.error("Parsing failed: String to order, string: {}", s);
            throw new RuntimeException(exp);
        }
    }
}
