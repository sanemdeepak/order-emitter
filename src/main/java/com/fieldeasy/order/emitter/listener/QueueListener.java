package com.fieldeasy.order.emitter.listener;

/**
 * Created by sanemdeepak on 12/12/18.
 */
public interface QueueListener<T> {
    void listen();

    T process(T order);

    void acknowledge(T messageId);
}
