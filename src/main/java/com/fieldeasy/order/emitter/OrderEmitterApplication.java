package com.fieldeasy.order.emitter;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.fieldeasy.order.emitter.util.BasicAWSCredentialProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;


/**
 * Created by sanemdeepak on 12/9/18.
 */
@SpringBootApplication
@RequiredArgsConstructor
public class OrderEmitterApplication {

    private final BasicAWSCredentialProvider basicAWSCredentialProvider;

    public static void main(String[] args) {
        SpringApplication.run(OrderEmitterApplication.class, args);
    }

    @Bean
    public Validator getValidator() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }


    @Bean
    public AmazonSQS getAmazonSQS() {
        return AmazonSQSClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(basicAWSCredentialProvider)
                .build();
    }

    @Bean
    public AmazonS3 getAmazonS3() {
        return AmazonS3ClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(basicAWSCredentialProvider)
                .build();
    }
}
