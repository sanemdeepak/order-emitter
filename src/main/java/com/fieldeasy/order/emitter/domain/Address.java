package com.fieldeasy.order.emitter.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by sanemdeepak on 12/9/18.
 */
@Data
public class Address implements Serializable {

    @NotBlank
    @Size(max = 25)
    private String lineOne;

    @Size(max = 25)
    private String lineTwo;

    @NotBlank
    @Size(max = 25)
    private String city;

    @NotBlank
    @Size(min = 6, max = 6)
    private String pin;

    @NotNull
    private State state;

}
