package com.fieldeasy.order.emitter.domain;

import java.io.Serializable;

/**
 * Created by sanemdeepak on 2018-12-13.
 */
public enum Status implements Serializable {
    PENDING,
    PROCESSING,
    CLOSED

}
