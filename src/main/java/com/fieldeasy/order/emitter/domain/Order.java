package com.fieldeasy.order.emitter.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/9/18.
 */
@Data
@Document(collection = "Order")
public class Order implements Serializable {

    @JsonIgnore
    @Id
    private UUID id;

    @JsonIgnore
    private UUID forClient;

    //Order details
    @NotNull
    @Valid
    private ServiceType serviceType;

    @NotNull
    @Valid
    private Address address;

    @NotBlank
    private String serviceDescription;

    @NotBlank
    private String contactName;

    @NotBlank
    private String contactPhoneNumber;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date expectedFulfilmentDate;

    private Status status;

    //Audit fields
    @JsonIgnore
    private Instant orderDate;

    @JsonIgnore
    private Instant updatedOn;
}
