package com.fieldeasy.order.emitter.domain.response;

import lombok.Data;

/**
 * Created by sanemdeepak on 12/9/18.
 */
@Data
public class ServiceResponse {
    private String requestId;
    private Long timestamp;
    private Status status;
    private Object data;
    private Object message;
    private String code;
}
