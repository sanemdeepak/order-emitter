package com.fieldeasy.order.emitter.domain;

import lombok.Data;

/**
 * Created by sanemdeepak on 2019-02-19.
 */
@Data
public class OrderMessage {
    private final String header;
    private final Order order;
}
