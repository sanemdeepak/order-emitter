package com.fieldeasy.order.emitter.controller;

import com.fieldeasy.order.emitter.domain.Order;
import com.fieldeasy.order.emitter.domain.OrderMessage;
import com.fieldeasy.order.emitter.domain.response.ServiceResponse;
import com.fieldeasy.order.emitter.domain.response.Status;
import com.fieldeasy.order.emitter.handler.Handler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/9/18.
 */
@RestController
@RequestMapping("/order")
@Slf4j
@RequiredArgsConstructor
public class OrderController {

    private final Handler<OrderMessage, Order> orderHandler;
    private final Validator validator;


    @PostMapping
    public ServiceResponse newOrder(@RequestBody Order order) {
        MDC.put("requestId", UUID.randomUUID().toString());

        log.info("received order: {}", order.toString());

        this.validateOrder(order);
        OrderMessage orderMessage = new OrderMessage("New Order", order);

        try {
            Order processedOrder = orderHandler.handle(orderMessage);
            ServiceResponse successRes = new ServiceResponse();
            successRes.setTimestamp(Instant.now().toEpochMilli());
            successRes.setStatus(Status.SUCCESS);
            successRes.setData(processedOrder);
            log.info("Successfully processed order: {}", order.toString());
            return successRes;
        } catch (Exception exp) {
            log.error("Error processing order: {} with cause: {}", order, ExceptionUtils.getStackTrace(exp));
            ServiceResponse errorRes = new ServiceResponse();
            errorRes.setTimestamp(Instant.now().toEpochMilli());
            errorRes.setStatus(Status.ERROR);
            errorRes.setMessage(exp.getMessage());
            return errorRes;
        }
    }

    private void validateOrder(Order order) {

        Set<ConstraintViolation<Order>> exceptions = validator.validate(order);

        if (!exceptions.isEmpty()) {

            throw new ConstraintViolationException(exceptions);
        }
    }
}
