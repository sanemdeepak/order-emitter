# Order - Emitter

A web service, responsible for consuming order via rest API and emitting for further processing

# Order - Emitter is responsible for

* Generating Order Id.
* Maintaining Order details such as audit fields, createdOn and updatedOn dates.
* Updating Order Status

